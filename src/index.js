import express from 'express';
import morgan from 'morgan';
import db from './config/knexfile';
import routes from './api/routes';
import errorHandlerMiddleware from './api/middleware/errorHandlerMiddleware';
import authorizationMiddleware from './api/middleware/authorizationMiddleware';
import logger, { logStream } from './config/loggerConfig';
import serverConfig from './config/serverConfig';

const { port } = serverConfig.development;

const app = express();

db.raw('select 1+1 as result').then(() => {
  logger.info('Connected to the database.');
}).catch(err => {
  logger.error('Could not connect to the database.', err);
  throw new Error('No db connection');
});

app.use(express.json());
app.use(morgan('combined', { stream: logStream }));

app.use('/', authorizationMiddleware);
routes(app);

app.use(errorHandlerMiddleware);

app.listen(port, () => {
  logger.info(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };
