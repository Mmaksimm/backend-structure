import { Router } from 'express';
import validatingUserMiddleware from '../middleware/validatingUserMiddleware';
import validatingAdminMiddleware from '../middleware/validatingAdminMiddleware';
import * as transactionService from '../services/transactionService';

const router = Router();

router
  .post('/',
    validatingAdminMiddleware,
    validatingUserMiddleware, (req, res, next) => transactionService.createTransaction(req)
      .then(createdTransaction => res.send(createdTransaction))
      .catch(next));

export default router;
