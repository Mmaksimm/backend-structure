import { Router } from 'express';
import validatingEventMiddleware from '../middleware/validatingEventMiddleware';
import validatingAdminMiddleware from '../middleware/validatingAdminMiddleware';
import validatingUpdateEventMiddleware from '../middleware/validatingUpdateEventMiddleware';

import * as eventService from '../services/eventService';

const router = Router();

router
  .post('/',
    validatingAdminMiddleware,
    validatingEventMiddleware,
    (req, res, next) => eventService.createEvent(req.body)
      .then(createdEvent => res.send(createdEvent))
      .catch(next))
  .put('/:id',
    validatingUpdateEventMiddleware,
    (req, res, next) => eventService.updateEvent(req.params.id, req.body.score)
      .then(updatedEvent => res.send(updatedEvent))
      .catch(next));

export default router;
