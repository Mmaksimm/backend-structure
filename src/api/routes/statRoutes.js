import { Router } from 'express';
import * as statService from '../services/statService';

const router = Router();

router
  .get('/', (req, res, next) => statService.getStats(req)
    .then(stats => res.send(stats))
    .catch(next));

export default router;
