import { Router } from 'express';
import validatingBetMiddleware from '../middleware/validatingBetMiddleware';
import * as betService from '../services/betService';

const router = Router();

router
  .post('/',
    validatingBetMiddleware,
    (req, res, next) => betService.createBet(req.body, req.user)
      .then(createdBet => res.send(createdBet))
      .catch(next));

export default router;
