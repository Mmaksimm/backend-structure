import { Router } from 'express';
import validatingIdMiddleware from '../middleware/validatingIdMiddleware';
import validatingUserMiddleware from '../middleware/validatingUserMiddleware';
import validatingUpdateUserMiddleware from '../middleware/validatingUpdateUserMiddleware';
import userIdMismatchMiddleware from '../middleware/userIdMismatchMiddleware';
import * as userService from '../services/userService';

const router = Router();

router
  .get('/:id', validatingIdMiddleware, (req, res, next) => userService.getUserById(req.params.id)
    .then(user => res.send(user))
    .catch(next))
  .post('/', validatingUserMiddleware, (req, res, next) => userService.createUser(req.body)
    .then(createdUser => res.send(createdUser))
    .catch(next))
  .put('/:id',
    validatingUpdateUserMiddleware,
    userIdMismatchMiddleware,
    (req, res, next) => userService.updateUser(req.params.id, req.body)
      .then(updatedUser => res.send(updatedUser))
      .catch(next));

export default router;
