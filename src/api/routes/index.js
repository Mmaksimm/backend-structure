import healthRoutes from './healthRoutes';
import userRoutes from './userRoutes';
import transactionRoutes from './transactionRoutes';
import eventRoutes from './eventRoutes';
import betRoutes from './betRoutes';
import statRoutes from './statRoutes';
import RoutesName from '../../constants/enums/RoutesName';

// register all routes
export default app => {
  app.use(RoutesName.Health, healthRoutes);
  app.use(RoutesName.Users, userRoutes);
  app.use(RoutesName.Transactions, transactionRoutes);
  app.use(RoutesName.Events, eventRoutes);
  app.use(RoutesName.Bets, betRoutes);
  app.use(RoutesName.Stats, statRoutes);
};
