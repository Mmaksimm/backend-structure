import NotFoundErrors from '../../errors/notFoundErrors';
import InternalServerError from '../../errors/internalServerError';
import oddsRepository from '../../db/repositories/oddsRepository';

const getOddsById = async id => {
  const [odds] = await oddsRepository.getById(id);
  if (!odds) {
    throw new NotFoundErrors('Odds not found');
  }
  return odds;
};

const createOdds = async newOdds => {
  try {
    const [odds] = await oddsRepository.create(newOdds);
    return odds;
  } catch (err) {
    throw new InternalServerError();
  }
};

export {
  getOddsById,
  createOdds
};
