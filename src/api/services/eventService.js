/* eslint-disable no-param-reassign */
import NotFoundErrors from '../../errors/notFoundErrors';
import * as oddsService from './oddsService';
import * as betService from './betService';
import * as userService from './userService';
import eventRepository from '../../db/repositories/eventRepository';
import StatsFieldName from '../../constants/enums/StatsFieldName';
import getResult from '../../helpers/getResult';

const getEventById = async id => {
  const [event] = await eventRepository.getById(id);
  if (!event) throw new NotFoundErrors('Event not found');
  return event;
};

const createEvent = async newEvent => {
  const [odds] = await oddsService.createOdds(newEvent.odds);
  const [event] = await eventRepository.create({ ...newEvent, oddsId: odds.id });
  return { ...event, ...odds };
};

const updateEvent = async (eventId, score) => {
  const bets = await betService.getBetByEventIdWhereWinNull(eventId);

  const result = getResult(score);

  const [event] = await eventRepository.updateById(eventId, { score });

  await Promise.all(bets.map(async bet => {
    if (bet.prediction === result) {
      await betService.updateBet(bet.id, { win: true });
      const user = await userService.getUserById(bet.userId);
      const balance = user.balance + (bet.betAmount * bet.multiplier);
      await userService.updateUser(bet.userId, { balance });
    } else if (bet.prediction !== result) {
      await betService.updateBet(bet.id, { win: false });
    }
  }));

  return event;
};

const getAmountEventsOfId = async () => {
  const [totalEvents] = await eventRepository.getAmountOfId(StatsFieldName.TotalEvents);
  return totalEvents;
};

export {
  getEventById,
  createEvent,
  updateEvent,
  getAmountEventsOfId
};
