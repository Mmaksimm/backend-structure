import * as userService from './userService';
import * as betService from './betService';
import * as eventService from './eventService';

export const getStats = async () => {
  const totalUsers = await userService.getAmountUsersOfId();
  const totalBets = await betService.getAmountBetsOfId();
  const totalEvents = await eventService.getAmountEventsOfId();
  return {
    ...totalUsers,
    ...totalBets,
    ...totalEvents
  };
};
