/* eslint-disable no-param-reassign */
import jwt from 'jsonwebtoken';
import NotFoundErrors from '../../errors/notFoundErrors';
import InternalServerError from '../../errors/internalServerError';
import BadRequestError from '../../errors/badRequestError';
import userRepository from '../../db/repositories/userRepository';
import StatsFieldName from '../../constants/enums/StatsFieldName';

const getUserById = async id => {
  const [user] = await userRepository.getById(id);

  if (!user) {
    throw new NotFoundErrors('User not found');
  }
  return user;
};

const createUser = async newUser => {
  try {
    const balance = 0;
    const [createdUser] = await userRepository.create(newUser);
    return {
      ...createdUser,
      balance,
      accessToken: jwt.sign({ id: createdUser.id, type: createdUser.type }, process.env.JWT_SECRET)
    };
  } catch (err) {
    if (err.code === '23505') {
      throw new BadRequestError(err.detail);
    }
    throw new InternalServerError();
  }
};

const updateUser = async (userId, updateUserData) => {
  try {
    const [updatedUser] = await userRepository.updateById(userId, updateUserData);
    return updatedUser;
  } catch (err) {
    if (err.code === 23505) {
      throw new BadRequestError(err.detail);
    }
    throw new InternalServerError();
  }
};

const getAmountUsersOfId = async () => {
  try {
    const [totalUsers] = await userRepository.getAmountOfId(StatsFieldName.TotalUser);
    return totalUsers;
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err);
    throw new InternalServerError();
  }
};

export {
  getUserById,
  createUser,
  updateUser,
  getAmountUsersOfId
};
