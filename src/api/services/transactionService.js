import BadRequestError from '../../errors/badRequestError';
import transactionRepository from '../../db/repositories/transactionRepository';
import * as userService from './userService';

const createTransaction = async transactionData => {
  const user = await userService.getUserById(transactionData.userId);
  if (!user) throw new BadRequestError('User does not exist');
  const [transaction] = await transactionRepository.create(transactionData);
  const currentBalance = transactionData.amount + user.balance;
  return { ...transaction, currentBalance };
};

export {
  createTransaction
};
