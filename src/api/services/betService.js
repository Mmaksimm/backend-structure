import NotFoundErrors from '../../errors/notFoundErrors';
import BadRequestError from '../../errors/badRequestError';
import eventRepository from '../../db/repositories/eventRepository';
import * as oddsService from './oddsService';
import * as userService from './userService';
import betRepository from '../../db/repositories/betRepository';
import StatsFieldName from '../../constants/enums/StatsFieldName';
import getMultiplier from '../../helpers/getMultiplier';

const getBetByEventIdWhereWinNull = async betId => {
  const [bets] = await betRepository.getBetByEventIdWhereWinNull(betId);
  if (!bets) throw new NotFoundErrors('User not found');
  return bets;
};

const createBet = async (newBet, { id: userId }) => {
  const user = await userService.getUserById(userId);
  const currentBalance = user.balance - newBet.betAmount;

  if (currentBalance <= 0) {
    throw new BadRequestError('Not enough balance');
  }

  const event = await eventRepository.getById(newBet.eventId);
  const odds = await oddsService.getOddsById(event.oddsId);

  const multiplier = getMultiplier(newBet, odds);

  const [bet] = betRepository.create({
    ...newBet,
    multiplier,
    eventId: event.id
  });

  await userService.updateUser(userId, { balance: currentBalance });
  return {
    ...bet,
    currentBalance
  };
};

const updateBet = async (betId, updateBetData) => {
  const [updatedBet] = await betRepository.updateById(betId, updateBetData);
  return updatedBet;
};

const getAmountBetsOfId = async () => {
  const [totalBets] = await betRepository.getAmountOfId(StatsFieldName.TotalBets);
  return totalBets;
};

export {
  getBetByEventIdWhereWinNull,
  createBet,
  updateBet,
  getAmountBetsOfId
};
