import logger from '../../config/loggerConfig';

export default (err, req, res, next) => {
  logger.error(err);
  if (res.headersSent) {
    next(err);
  } else {
    const { status = 500, message = '' } = err;
    res.status(status).send({ status, message });
  }
};
