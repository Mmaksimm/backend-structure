import joi from 'joi';
import BadRequestError from '../../errors/badRequestError';

const validatingId = ({ params }, res, next) => {
  const schema = joi.object({
    id: joi.string().uuid()
  }).required();

  const isValidResult = schema.validate(params);

  if (isValidResult.error) {
    throw new BadRequestError(isValidResult.error.details[0].message);
  }
  next();
};

export default validatingId;
