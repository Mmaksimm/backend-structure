import joi from 'joi';
import BadRequestError from '../../errors/badRequestError';

const validatingUserMiddleware = ({ body: user }, res, next) => {
  const schema = joi.object({
    id: joi.string().uuid(),
    type: joi.string().required(),
    email: joi.string().email().required(),
    phone: joi.string().pattern(/^\+?3?8?(0\d{9})$/).required(),
    name: joi.string().required(),
    city: joi.string()
  }).required();

  const isValidResult = schema.validate(user);
  if (isValidResult.error) {
    throw new BadRequestError(isValidResult.error.details[0].message);
  }
  next();
};

export default validatingUserMiddleware;
