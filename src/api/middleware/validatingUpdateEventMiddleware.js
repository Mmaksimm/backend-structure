import joi from 'joi';
import BadRequestError from '../../errors/badRequestError';

const validatingUpdateEventMiddleware = ({ body: event }, res, next) => {
  const schema = joi.object({
    score: joi.string().required()
  }).required();

  const isValidResult = schema.validate(event);

  if (isValidResult.error) {
    BadRequestError(isValidResult.error.details[0].message);
  }
  next();
};

export default validatingUpdateEventMiddleware;
