import joi from 'joi';
import BadRequestError from '../../errors/badRequestError';

const validatingUpdateUserMiddleware = ({ body: user }, res, next) => {
  const schema = joi.object({
    email: joi.string().email(),
    phone: joi.string().pattern(/^\+?3?8?(0\d{9})$/),
    name: joi.string(),
    city: joi.string()
  }).required();
  const isValidResult = schema.validate(user);
  if (isValidResult.error) {
    throw new BadRequestError(isValidResult.error.details[0].message);
  }
  next();
};

export default validatingUpdateUserMiddleware;
