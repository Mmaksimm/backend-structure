import UnauthorizedError from '../../errors/unauthorizedError';

const validatingAdminMiddleware = ({ user }, res, next) => {
  if (user.type !== 'admin') {
    throw new UnauthorizedError();
  }
  next();
};

export default validatingAdminMiddleware;
