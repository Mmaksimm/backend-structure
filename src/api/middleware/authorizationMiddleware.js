import jwt from 'jsonwebtoken';
import routesWhiteList from '../../helpers/routesWhiteList';
import UnauthorizedError from '../../errors/unauthorizedError';

const authorizationMiddleware = (req, res, next) => {
  try {
    if (!routesWhiteList(req)) {
      const token = req.headers.authorization.replace('Bearer ', '');
      const user = jwt.verify(token, process.env.JWT_SECRET);
      req.user = user;
    }
    next();
  } catch (err) {
    throw new UnauthorizedError('Not Authorized');
  }
};

export default authorizationMiddleware;
