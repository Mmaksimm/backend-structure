import joi from 'joi';
import BadRequestError from '../../errors/badRequestError';

const validatingBetMiddleware = ({ body: bet }, res, next) => {
  const schema = joi.object({
    id: joi.string().uuid(),
    eventId: joi.string().uuid().required(),
    betAmount: joi.number().min(1).required(),
    prediction: joi.string().valid('w1', 'w2', 'x').required()
  }).required();

  const isValidResult = schema.validate(bet);

  if (isValidResult.error) {
    BadRequestError(isValidResult.error.details[0].message);
  }
  next();
};

export default validatingBetMiddleware;
