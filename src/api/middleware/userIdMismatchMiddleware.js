import UnauthorizedError from '../../errors/unauthorizedError';

const userIdMismatchMiddleware = (req, res, next) => {
  if (req.params.id !== req.user.id) {
    throw new UnauthorizedError('UserId mismatch');
  }
  next();
};

export default userIdMismatchMiddleware;
