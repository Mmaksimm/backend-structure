import BaseError from './baseError';
import HttpStatus from '../constants/enums/HttpStatus';

class InternalServerError extends BaseError {
  constructor(message) {
    super(HttpStatus.InternalServerError.status);
    this.message = message || HttpStatus.InternalServerError.message;
  }
}

export default InternalServerError;
