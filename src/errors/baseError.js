class BaseError extends Error {
  constructor(status) {
    super();
    this.status = status;
  }
}

export default BaseError;
