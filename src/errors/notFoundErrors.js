import BaseError from './baseError';
import HttpStatus from '../constants/enums/HttpStatus';

class NotFoundError extends BaseError {
  constructor(message) {
    super(HttpStatus.NotFound.status);
    this.message = message || HttpStatus.NotFound.message;
  }
}

export default NotFoundError;
