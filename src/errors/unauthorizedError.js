import BaseError from './baseError';
import HttpStatus from '../constants/enums/HttpStatus';

class UnauthorizedError extends BaseError {
  constructor(message) {
    super(HttpStatus.Unauthorized.status);
    this.message = message || HttpStatus.Unauthorized.message;
  }
}

export default UnauthorizedError;
