import BaseError from './baseError';
import HttpStatus from '../constants/enums/HttpStatus';

class BadRequestError extends BaseError {
  constructor(message) {
    super(HttpStatus.BadRequest.status);
    this.message = message || HttpStatus.BadRequest.message;
  }
}

export default BadRequestError;
