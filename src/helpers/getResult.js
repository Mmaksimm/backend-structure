const getResult = score => {
  const [w1, w2] = score.split(':');

  const diff = w1 - w2;
  if (diff > 0) return 'w1';
  if (diff < 0) return 'w2';
  return 'x';
};

export default getResult;
