const getMultiplier = (newBet, odds) => {
  switch (newBet.prediction) {
    case 'w1':
      return odds.homeWin;

    case 'w2':
      return odds.awayWin;

    case 'x':
      return odds.draw;
    default:
      return undefined;
  }
};

export default getMultiplier;
