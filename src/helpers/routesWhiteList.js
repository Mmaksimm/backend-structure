/* eslint-disable no-console */
import RoutesName from '../constants/enums/RoutesName';
import MethodName from '../constants/enums/MethodName';

const routesWhiteList = req => {
  if (req.path === RoutesName.Health) return true;
  const usersPath = (req.path.match(/^\/[^/]*/gi)[0] === RoutesName.Users);
  const methodGetPost = ((req.method === MethodName.GET || req.method === MethodName.POST));
  if (usersPath && methodGetPost) return true;
  return false;
};

export default routesWhiteList;
