import dotenv from 'dotenv';

dotenv.config();
const defaultAppPort = 3000;

const serverConfig = {
  development: {
    // It don't work for me. It starts 2 server instances.
    port: /* process.env.APP_PORT || */ defaultAppPort
  }
};

export default serverConfig;
