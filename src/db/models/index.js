import db from '../../config/knexfile';
import ModelsName from '../../constants/enums/ModelsName';

const BetModel = db(ModelsName.Bet);
const EventModel = db(ModelsName.Event);
const OddsModel = db(ModelsName.Odds);
const UserModel = db(ModelsName.User);
const TransactionModel = db(ModelsName.Transaction);

export {
  BetModel,
  EventModel,
  OddsModel,
  UserModel,
  TransactionModel
};
