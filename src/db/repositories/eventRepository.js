import { EventModel } from '../models';
import BaseRepository from './baseRepository';

class EventRepository extends BaseRepository { }

export default new EventRepository(EventModel);
