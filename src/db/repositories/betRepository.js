import { BetModel } from '../models';
import BaseRepository from './baseRepository';

class BetRepository extends BaseRepository {
  getBetByEventIdWhereWinNull(betId) {
    return this.model.where('eventId', betId).andWhere('win', null);
  }
}

export default new BetRepository(BetModel);
