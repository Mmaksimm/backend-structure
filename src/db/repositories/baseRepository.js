export default class BaseRepository {
  constructor(model) {
    this.model = model;
  }

  getAll() {
    return this.model.findAll();
  }

  getById(id) {
    return this.model.where('id', id).returning('*');
  }

  create(data) {
    return this.model.insert(data).returning('*');
  }

  async updateById(id, data) {
    return this.model.where('id', id).update(data).returning('*');
  }

  deleteById(id) {
    return this.model.where('id', id).del();
  }

  getAmountOfId(name) {
    return this.model.count('id', { as: name });
  }
}
