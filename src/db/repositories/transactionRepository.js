import { TransactionModel } from '../models';
import BaseRepository from './baseRepository';

class TransactionRepository extends BaseRepository {
  createTransaction(transaction) {
    return this.model.insert(transaction).returning('*');
  }
}

export default new TransactionRepository(TransactionModel);
