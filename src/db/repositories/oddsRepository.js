import { OddsModel } from '../models';
import BaseRepository from './baseRepository';

class OddsRepository extends BaseRepository { }

export default new OddsRepository(OddsModel);
