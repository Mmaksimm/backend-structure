const ModelsName = {
  Bet: 'bet',
  Event: 'event',
  Odds: 'odds',
  User: 'user',
  Transaction: 'transaction'
};

export default ModelsName;
