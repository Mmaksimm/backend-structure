const RoutesName = {
  Health: '/health',
  Users: '/users',
  Transactions: '/transactions',
  Events: '/events',
  Stats: '/stats',
  Bets: '/bets'
};

export default RoutesName;
