const StatsFieldName = {
  TotalBets: 'totalBets',
  TotalUsers: 'totalUsers',
  TotalEvents: 'totalEvents'
};

export default StatsFieldName;
